#include <WinSock2.h>
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#pragma comment(lib, "ws2_32.lib")

#define TARGET_ADDR "127.0.0.1"
#define TARGET_PORT 20810

SOCKET udpsock = INVALID_SOCKET;
sockaddr_in udpsa;
sockaddr_in svsa;

int SV_SendData(char* buf, int len, sockaddr_in sa)
{
	return sendto(udpsock, buf, len, NULL, (sockaddr*)&sa, sizeof(sa));
}

bool SV_ResolveNameserver(const char* nameserver, const char** outIP)
{
	hostent* h = gethostbyname(nameserver);
	if(!h) return false;

	in_addr addr;
	addr.s_addr = *(ULONG*)h->h_addr_list[0];

	*outIP = inet_ntoa(addr);

	return true;
}

void SV_SendDPInfoResponse(char* challenge, sockaddr_in sa)
{
	printf("Sending info response...\n");

	char buf[2048];

	_snprintf(buf, 2018, "\xff\xff\xff\xffinfoResponse\\hc\\0\\npid\\ 130000100000CD9\\shortversion\\3.0-1\\fs_game\\mods/tsd\\pure\\1\\gametype\\war\\sv_maxclients\\18\\sv_privateClients\\0\\clients\\0\\mapname\\mp_afghan\\protocol\\61586\\gamename\\IW4\\hostname\\life is not easy :(\\challenge\\%s",
					challenge);

	printf("%s\n", buf);

	if(SV_SendData(buf, strlen(buf)+1, sa) == SOCKET_ERROR)
	{
		printf("Failed to send data, %d.\n", WSAGetLastError());
	}
}

void SV_HandleDPPacketData(char* buf, int len)
{
	if(*(int*)buf == 0xFFFFFFFF)
	{
		buf += 4, len -= 4;

		if(strstr(buf, "getinfo "))
		{
			char *challenge = &buf[8];
			
			printf("Received getinfo DP packet with challenge \"%s\".\n", challenge);

			SV_SendDPInfoResponse(challenge, udpsa);
		}

		return;
	}

	printf("Invalid packet signature.\n");
}

void SV_HandleCLPacketData(char* buf, int len, sockaddr_in clisa)
{
	if(*(int*)buf == 0xFFFFFFFF)
	{
		buf += 4, len -= 4;

		if(strstr(buf, "getinfo "))
		{
			char *challenge = &buf[8];

			int size = challenge[0] == 'x' ? 3 : 8;

			
			printf("Received getinfo CL packet with challenge \"%s\".\n", challenge);

			SV_SendDPInfoResponse(challenge, clisa);
		}

		return;
	}

	printf("Invalid packet signature.\n");
}

DWORD WINAPI SV_ReceiveDPDataThread(LPVOID lpParam)
{
	char buf[4028];
	int udpsalen = sizeof(udpsa);

	int len = recvfrom(udpsock, buf, sizeof(buf), NULL,
				(sockaddr*)&udpsa, &udpsalen);

	while(len != SOCKET_ERROR)
	{
		if(len > NULL)
		{
			printf("Received %d bytes from DP.\n", len);

			buf[len] = '\0';
			SV_HandleDPPacketData(buf, len);			
		}

		len = recvfrom(udpsock, buf, sizeof(buf), NULL,
				(sockaddr*)&udpsa, &udpsalen);
	}

	printf("Failed to receive, %d\n", WSAGetLastError());

	return NULL;
}

DWORD WINAPI SV_ReceiveCLDataThread(LPVOID lpParam)
{
	char buf[256];

	sockaddr_in clisa;
	int clisalen = sizeof(clisa);

	int len = recvfrom(udpsock, buf, sizeof(buf), NULL,
				(sockaddr*)&clisa, &clisalen);

	while(len != SOCKET_ERROR)
	{
		if(len > NULL)
		{
			printf("Received %d bytes.\n", len);

			buf[len] = '\0';
			SV_HandleCLPacketData(buf, len, clisa);
		}

		len = recvfrom(udpsock, buf, sizeof(buf), NULL,
				(sockaddr*)&clisa, &clisalen);
	}
	
	printf("Failed to receive, %d\n", WSAGetLastError());

	return NULL;
}

bool SV_InitSettings()
{
	WSAStartup(MAKEWORD(2,2), new WSADATA);
	udpsock = socket(AF_INET, SOCK_DGRAM, NULL);

	if(udpsock == INVALID_SOCKET)
	{
		printf("Failed to create UDP socket.\n");
		return false;
	}
	
	memset(&svsa, NULL, sizeof(svsa));
	memset(&udpsa, NULL, sizeof(udpsa));

	return true;
}

DWORD WINAPI SV_HeartbeatThread(LPVOID lpParam)
{
	while(true)
	{
		char buf[32];
		_snprintf(buf, 32, "\xff\xff\xff\xffheartbeat IW4");

		printf("Sending heartbeat for game IW4...\n");

		if(SV_SendData(buf, strlen(buf), udpsa) == SOCKET_ERROR)
		{
			printf("Failed to send buffer, %d.\n", WSAGetLastError());
			return false;
		}

		Sleep(120000);
	}

	return NULL;
}

bool SV_StartHeartbeat(const char* addr, int port)
{
	const char* resolvedIP;
	SV_ResolveNameserver(addr, &resolvedIP);

	printf("Resolved %s to %s.\n", addr, resolvedIP);

	udpsa.sin_addr.s_addr = inet_addr(resolvedIP);
	udpsa.sin_port = htons(port);
	udpsa.sin_family = AF_INET;

	CreateThread(0, 0, SV_HeartbeatThread, 0, 0, 0);

	return true;
}

void SV_StartReceiving()
{
	CreateThread(0, 0, SV_ReceiveDPDataThread, 0, 0, 0);
}

bool SV_StartDediServer(int port)
{
	if(port <= 0) return false;

	printf("Starting dedicated server on port %d...\n", port);
	
	svsa.sin_addr.s_addr = INADDR_ANY;
	svsa.sin_port = htons(port);
	svsa.sin_family = AF_INET;

	if(bind(udpsock, (sockaddr*)&svsa, sizeof(svsa)) == SOCKET_ERROR)
	{
		printf("Failed to bind UDP socket, %d.\n", WSAGetLastError());
		return false;
	}

	CreateThread(0, 0, SV_ReceiveCLDataThread, 0, 0, 0);

	return true;
}

int main(int argc, char** argv)
{
	SV_InitSettings();
	SV_StartHeartbeat(TARGET_ADDR, TARGET_PORT);
	SV_StartReceiving();
	SV_StartDediServer(28960);

	getchar();
	return NULL;
}